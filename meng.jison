/* Meng grammar by Deofly */

%{
	function check_var(v) {
		if (_VAR[v])	return true;
		else			return false;
	} 
%}

%lex
%%

\s+		                            	{/* skip whitespace */}
"+"										{ return '+'; }
"-"										{ return '-'; }
"*" 									{ return '*'; }
"/"										{ return '/'; }
"%"										{ return '%'; }
"!"										{ return '!'; }
"("										{ return '('; }
")"										{ return ')'; }
"="										{ return '='; }
','										{ return ','; }
"input"             	   	    	    { return 'INPUT'; }
"click"									{ return 'CLICK'; }
"sleep"									{ return 'SLEEP'; }
"refresh"								{ return 'REFRESH'; }
"at"									{ return 'AT'; }
"after" 								{ return 'AFTER'; }
"20"([0-9][0-9]"-"(("0"[1-9]|"1"[0-2])"-"("0"[1-9]|"1"[0-9]|"2"[0-8])|("0"[13-9]|"1"[0-2])"-"("29"|"30")|("0"[13578]|"1"[02])"-31")|([02468][048]|[13579][26])"-02-29")					{ return 'DATE'; /* 2000-2099 */ }
([01][0-9]|"2"[0-3])":"[0-5][0-9]":"[0-5][0-9]		{ return 'TIME'; }
[0-9]+									{ return 'DIGIT'; }
[_a-zA-Z][_a-zA-Z0-9]*					{ return 'VAR'; }
\"([^\\\"\n\r\t]|"\\\""|"\\\\")*\"		{ return 'STRING'; }
<<EOF>>                         		{ return 'EOF'; }

/lex

/* operator associations and precedence */

%left '+' '-'
%left '*' '/' '%'
%left '^'
%left UMINUS

%start main_

%% /* language grammar */

main_
	: main EOF
	;

main
	: assignment_statement main
	| input_statement main
	| click_statement main
	| sleep_statement main
	| refresh_statement main
	| 
	; 

assignment_statement
	: var '=' assignment_statement
		{ $$ = $3; _VAR[$1] = $3; console.log($1 + ' = ' + $3); }
	| var '=' v_list
		{ $$ = $3; _VAR[$1] = $3; console.log($1 + ' = ' + $3); }
	;

input_statement
	: var INPUT expression
		%{ 
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input') {
				var delay = _DELAY;
				setTimeout(function() {
					execute('input', tid, eid, String($3));
				}, delay);
			}
			else {
				console.error($1 + ' not support input!');
			}
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| var INPUT expression AT time
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input') {
				var now = new Date;
				var delay = $5.getTime() - now.getTime();
				setTimeout(function() { 
					execute("input", tid, eid, String($3)); 
				}, delay);
			}
			else {
				console.error($1 + ' not support input!');
			}
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| var INPUT expression AFTER expression
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input') {
				var delay = Number($5) + _DELAY;
				setTimeout(function(){ 
					execute("input", tid, eid, String($3)); 
				}, delay);
			}
			else {
				console.error($1 + ' not support input!');
			}
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	;

click_statement
	: var CLICK
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input' || type == 'button') {
				var delay = _DELAY;
				setTimeout(function() {
					execute('click', tid, eid);
				}, delay);
			}	
			else {
				console.error($1 + ' not support input!');
			}
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| var CLICK AT time
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input' || type == 'button') {
				var now = new Date;
				var delay = $4.getTime() - now.getTime();
				setTimeout(function() { 
					execute("click", tid, eid); 
				}, delay);
			}
			else {
				console.error($1 + ' not support input!');
			}
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| var CLICK AFTER expression
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var eid = e.eid;
			var type = e.type;
			if (type == 'input' || type == 'button') {
				var delay = Number($4) + _DELAY;
				setTimeout(function(){ 
					execute("click", tid, eid); 
				}, delay);
			}
			else {
				console.error($1 + ' not support input!');
			}	
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	;
sleep_statement 
	: SLEEP expression
		{ _DELAY += $2; }
	;

refresh_statement
	: var REFRESH
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var delay = _DELAY;
			setTimeout(function() {
				execute('reload', tid);
			}, delay);
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| num REFRESH
		{ setTimeout(function() { execute('reload', $1); }, _DELAY); }
	| var REFRESH AT time
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var now = new Date;
			var delay = $4.getTime() - now.getTime();
			setTimeout(function() { 
				execute("reload", tid); 
			}, delay);
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| num REFRESH AT time
		{ setTimeout(function() { execute('reload', $1); }, $4.getTime()-new Date().getTime()); }
	| var REFRESH AFTER expression
		%{
		if (elementList[$1]) {
			var e = elementList[$1];
			var tid = e.tid;
			var delay = Number($4) + _DELAY;
			setTimeout(function(){ 
				execute("reload", tid); 
			}, delay);
		}
		else {
			console.error($1 + ' undefined');
		}
		%}
	| num REFRESH AFTER expression
		{ setTimeout(function() { execute('reload', $1); }, Number($4)+_DELAY); }
	;

v_list
	: expression ',' v_list
		{ $$ = $3; $$.unshift($1); }
	| expression
		{ $$ = [$1]; }
	;

expression
	: expression '+' expression
		{ $$ = $1 + $3; }
	| expression '-' expression
		{ $$ = Number($1) - Number($3); }
	| expression '*' expression
		{ $$ = Number($1) * Number($3); }
	| expression '/' expression
		{ $$ = Number($1) / Number($3); }
	| expression '%' expression
		{ $$ = Number($1) % Number($3); }
	| expression '^' expression
		{ $$ = Math.pow(Number($1), Number($3)); }
	| '-' expression %prec UMINUS
		{ $$ = -Number($2); }
	| '(' expression ')'
		{ $$ = $2; }
	| num
		{ $$ = $1; }
	| str
		{ $$ = $1;}
	| var
		%{
		if (!check_var($1)) {
			console.error($1 + ' undefined');	
		}
		if (typeof($1).toLowerCase() != 'string' && typeof($1).toLowerCase() != 'number') {
			console.error($1 + ' type error');
		}
		$$ = _VAR[$1]; 
		%}
	;

var
	: VAR
		{ $$ = yytext; }
	;

str
	: STRING
		{ $$ = yytext.substr(1, yytext.length-2); $$ = $$.replace(/\\(?=(\\|\"))/g, ""); /* replace '\\' with '\' and replace '\"' with '"' */ }
	;


num
	: DIGIT
		{ $$ = Number(yytext); }
	;

time
	: TIME
		%{
		var t = String($1).split(':');
		var date = new Date;
		var h = date.getHours();
		var m = date.getMinutes();
		var s = date.getSeconds();
		if (t[0]*3600 + t[1]*60 + t[2] <= h*3600 + m*60 + s)
			date.setDate(date.getDate() + 1);
		date.setHours(t[0]);
		date.setMinutes(t[1]);
		date.setSeconds(t[2]);
		$$ = date;
		%}	
	| DATE TIME
		%{
		var d = String($1).split('-');
		var t = String($2).split(':');
		$$ = new Date(d[0], d[1]-1, d[2], t[0], t[1], t[2]);
		%}
	;




























