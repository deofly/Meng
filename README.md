Meng
====

Description: 
    A lightweight chrome-extension that makes surfing the Internet more convenient

For getting this, please:
    git clone git@github.com:deofly/Meng.git

Support grammar:
    <variable> = [<variable> = ...] <expression1> [, <expression2> ... ]
    <alias> input <string> [at <time> | after <microseconds>]
    <alias> click [at <time> | after <microseconds>]
    <alias> refresh [at <time> | after <microseconds>]
    sleep <microseconds>
