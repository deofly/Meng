/* redefine function 'elementFromPoint(x,y)' for compatible */
(function($) {
	var check = false,
		isRelative = true;
	$.elementFromPoint = function(x, y) {
		if (!document.elementFromPoint) return null;
		if (!check) {
			var sl;
			if ((sl = $(document).scrollTop()) > 0) {
				isRelative = (document.elementFromPoint(0, sl + $(window).height() - 1) == null);
			} else if ((sl = $(document).scrollLeft()) > 0) {
				isRelative = (document.elementFromPoint(sl + $(window).width() - 1, 0) == null);
			}
			check = (sl > 0);
		}
		if (!isRelative) {
			x += $(document).scrollLeft();
			y += $(document).scrollTop();
		}
		return document.elementFromPoint(x, y);
	}
})(jQuery);

/* define class and function */
function Point() {
	this.x = -1;
	this.y = -1;
	return this;
}
function Element() {
	this.id = "";
	this.type = "";
	return this;
}
function random16() {
	return ((1+Math.random())*0x10000 | 0).toString(16).substring(1);
}
function generateID() {
	return random16() + random16() + random16() + random16();
}

var pos = new Point();
$(document).mousedown(function(event) {
	if (event.which == 3) {
		pos.x = event.clientX;
		pos.y = event.clientY;
	}
});

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		var req = request.isCapture;
		if (req == 'true') {
			if (pos.x < 0 || pos.y < 0)
				return;
			var cur = $.elementFromPoint(pos.x, pos.y);
			var ele = new Element();
			ele.id = (cur.id) ? cur.id : generateID();
			cur.id = ele.id;
			ele.type = cur.tagName.toLowerCase();
			chrome.runtime.sendMessage(ele);
		}
	}
);