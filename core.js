var _VAR = new Array;
var _DELAY = 0;
var elementList = new Array;
var aliasList = new Array;

function run(val) {
	_VAR = [];
	_DELAY = 0;
	elementList = getAllElements();
	aliasList = [];
	for (var alias in elementList) {
		aliasList.push(alias);
	}
	var a = meng.parse(val);
}


function Element(wid, tid, url, eid, type) {
	this.wid = wid;
	this.tid = tid;
	this.url = url;
	this.eid = eid;
	this.type = type;
	return this;
}

function getAllElements() {
	var table = document.getElementById('items'); var elementList = new Array;
	for (var i = 0; i < table.rows.length; ++i) {
		var curRow = table.rows[i];
		var alias = curRow.cells[1].innerText;
		if (!alias || alias == '<Edit>')	continue;
		var wid = Number(curRow.cells[2].innerHTML);
		var tid = Number(curRow.cells[3].innerHTML);
		var url = curRow.cells[4].innerHTML;
		var eid = curRow.cells[5].innerHTML;
		var type = curRow.cells[6].innerHTML;
		var ele = new Element(wid, tid, url, eid, type);
		elementList[alias] = ele;
	};
	return elementList;
}

function execute(type, tid, eid, str) {
	switch(type) {
		case 'input':
			chrome.tabs.executeScript(tid, {code: 'document.getElementById("'+
				eid + '").value="' + str + '";'});
			console.log('tid:' + tid + ' eid:' + eid + ' event:[input] ' + str +
				' time:' + new Date);
			break;
		case 'click':
			chrome.tabs.executeScript(tid, {code: 'document.getElementById("'+
				eid + '").click();'})
			console.log('tid:' + tid + ' eid:' + eid + ' event:[click] time:' +
				new Date);
			break;
		case 'reload':
			chrome.tabs.reload(tid);
			console.log('tid:' + tid + ' event:[reload] time:' +
				new Date);
			break;
		default:
			console.log('function [execute] error!');
	}
		
}
