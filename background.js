
/* capture event triggered */
function onCapture(info, tab) {
	chrome.tabs.sendMessage(tab.id, {"isCapture": "true"});
}

chrome.contextMenus.create({
    	"type": "normal", 
    	"title": "Capture", 
    	"contexts": ["all"],
    	"onclick": onCapture
    }
);


/* focus or create IDE tab */
function focusOrCreateTab(url) {
  chrome.windows.getAll({"populate":true}, function(windows) {
    var existing_tab = null;
    var existing_win = null;
    var fs = url + '?fullscreen=yes';
    for (var i in windows) {
      existing_win = windows[i];
      var tabs = windows[i].tabs;
      for (var j in tabs) {
        var tab = tabs[j];
        if (tab.url == url || tab.url == fs) {
          existing_tab = tab;
          break;
        }
      }
    }
    if (existing_tab) {
      chrome.windows.update(existing_win.id, {"focused":true});
      chrome.tabs.update(existing_tab.id, {"highlighted":true});
    } else {
      chrome.tabs.create({"url":url, "selected":true});
    }
  });
}

chrome.browserAction.onClicked.addListener(function(tab) {
	var manager_url = chrome.extension.getURL("manager.html");
	focusOrCreateTab(manager_url);
});