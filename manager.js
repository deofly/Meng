
chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
        var t = $('#items');
        var len = t.find('tr').length;
		t.append('<tr>'+
			'<td>'+(len+1)+'</td>'+
			'<td class="editable">&lt;Edit&gt;</td>'+
			'<td>'+sender.tab.windowId+'</td>'+
			'<td>'+sender.tab.id+'</td>'+
			'<td>'+sender.url.split('?')[0]+'</td>'+
			'<td>'+request.id+'</td>'+
			'<td>'+request.type+'</td>'+
			'<td><a>delete</a></td>'+
			'</tr>');
	}
);

        
/* fullscreen */
window.onload = function() {
    var req = new Array();
    var s = location.search.substring(1);
    if (s) {
    	var list = s.split('&');
    	for (var i = 0; i < list.length; ++i)
    		var pair = list[i].split('=');
    	if (pair[0]) {
    		req[unescape(pair[0])] = unescape(pair[1]);
    	}
    }
    var fullscreen = req["fullscreen"];
    if (fullscreen != "yes") {
    	var file = self.location;
    	var a = window.open('about:blank', '', 
            'fullscreen=1,left=0,top=0,width='
            + (screen.availWidth-10) + ',height='
            + (screen.availHeight-50));
    	self.opener=null;
    	self.close();
    	a.location = file + "?fullscreen=yes";
    }
}

/* forbidden refresh */
document.onkeydown = function(ev) {
    var ev = ev || window.event;
    /* forbidden F5 */
    if (ev.keyCode == 116) {
        ev.keyCode = 0;
        return false;
    }
    /* forbidden Ctrl+R */
    if (ev.ctrlKey && ev.keyCode==82) {
        ev.keyCode = 0;
        return false;
    }
}
          
/* forbidden context menu */
document.oncontextmenu = function() {
    return false;
}


jQuery(document).ready(function($) {
    $('#items').dblclick(function(ev) {
        ev = ev || window.event;
        var obj = ev.target;
        if (obj.className == 'editable') {
            var inputItem = document.createElement('input');
            inputItem.type = 'text';
            inputItem.maxLength = 8;
            inputItem.value = (obj.innerHTML == '&lt;Edit&gt;') ? '' : obj.innerHTML;
            inputItem.onblur = function() {
                var v = inputItem.value;
                var pItem = inputItem.parentNode;
                pItem.removeChild(inputItem);
                var reg = /^[a-zA-z_][a-zA-z0-9_]*$/;
                if (v && !reg.test(v)) {
                    alert('Error: invalid variable!');
                    v = null;
                }
                pItem.innerHTML = (v) ? v : '&lt;Edit&gt;';
                pItem.style.color = (v) ? 'black' : 'gray';
            }
            obj.innerHTML = '';
            obj.appendChild(inputItem);
            inputItem.focus();
            //g_activeItem = inputItem;
        }
    });
    $('#items').click(function(ev) {
        ev = ev || window.event;
        var obj = ev.target;
        if (obj.tagName.toLowerCase() == 'a' && obj.innerHTML == 'delete') {
            var tr = obj.parentNode.parentNode;
            var index = parseInt(tr.cells[0].innerHTML);
            tr.parentNode.removeChild(tr);
            var len = this.rows.length;
            for (var i = index-1; i < len; ++i) {
                this.rows[i].cells[0].innerHTML = i+1;
            }
        }
    });

    $('#run').click(function(ev) {
        var val = $('#editarea').val();
        run(val);
    });
});

